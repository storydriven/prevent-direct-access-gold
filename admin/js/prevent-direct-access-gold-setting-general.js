(function($) {
    'use strict'
    var ajax_url = prevent_direct_access_gold_setting_data.ajax_url;
    var home_url = prevent_direct_access_gold_setting_data.home_url;

    $(document).ready(function() {
        if($('.pda-v3-gold-tooltip')) {
            if($('.pda-v3-gold-tooltip').tooltip) {
                $('.pda-v3-gold-tooltip').tooltip({
                    position: {
                        my: "left bottom-10",
                        at: "left top",
                    }
                });
            }
        }
    });

    $(function() {
        $('#pda_prefix_url').change(function(evt) {
            $('#pda_prefix').text(this.value);
        });

        $(".pda_select2").select2({
             width: '100%',
        });

        $(".pda_select2_for_role_protection").select2({
             width: '80%',
        });

        $("#pda_whitelist_role_non_expired_select2").select2();

        if ($("#file_access_permission").val() == 'custom_roles') {
            $('#grant-access').attr('required', true);
            $('#grant-access').show();
            $('#pda_role_select2').prop('required', true);
        } else {
            $('#grant-access').attr('required', false);
            $('#grant-access').hide();
            $('#pda_role_select2').prop('required', false);
        }

        $("#file_access_permission").change(function () {
            if ($("#file_access_permission").val() == 'custom_roles') {
                $('#grant-access').attr('required', true);
                $('#grant-access').show('slow');
                $('#pda_role_select2').prop('required', true);
            } else {
                $('#grant-access').attr('required', false);
                $('#grant-access').hide('slow');
                $('#pda_role_select2').prop('required', false);
            }
        });

        if ($("#pda_auto_protect_new_file").prop("checked")){
            $('#grant-access-protect-file').attr('required', true);
            $('#grant-access-protect-file').show();
        } else {
            $('#grant-access-protect-file').attr('required', false);
            $('#grant-access-protect-file').hide();
            $('#pda_auto_protect_new_file_select2').prop('required', false);
        }
        $("#pda_auto_protect_new_file").change(function() {
           if ($("#pda_auto_protect_new_file").prop("checked")){
               $('#grant-access-protect-file').attr('required', true);
               $('#grant-access-protect-file').show('slow');
           } else {
               $('#grant-access-protect-file').attr('required', false);
               $('#grant-access-protect-file').hide('slow');
               $('#pda_auto_protect_new_file_select2').prop('required', false);
           }
        });

        $("#pda_auto_replace_protected_file").change(function() {
            if($(this).prop('checked')) {
                $("#pda-pages-posts-replace").show('slow');
                $("#pda_replaced_pages_select2").prop('required', true);
            } else {
                $("#pda-pages-posts-replace").hide('slow');
                $("#pda_replaced_pages_select2").prop('required', false);
            }
        });

        _handle_fully_activate();

        $('#pda_setting_form').submit(function(evt) {
            evt.preventDefault();
            var replaced = $("#pda_prefix_url").val().trim().replace(/  +/g, '-');
            var result = replaced.split(' ').join('-');
            var title_page = $("#title_page_404_input").val();

            if($("#remote_log").prop('checked')== true) {
                $('#helpers').show();
            } else {
                $('#helpers').hide();
            }

            if(title_page != "") {
                $(".selected_page").text("Selected page: ");
                $("#remove_page").show();
                $('.remove-no-access-page').show();
                $(".no-access-selected-page-title").text(title_page);
                $(".no-access-selected-page-label").text('Selected page: ');
                $(".value_page").text(title_page);
                $('#pda_prefix').text(result);
            }
            _updateSettingsGeneral({
                // pda_enable_protection: $("#pda_enable_protection").prop('checked'),
                remote_log: $("#remote_log").prop('checked'),
                // pda_apply_logged_user: $("#pda_apply_logged_user").prop('checked'),
                pda_prefix_url: $("#pda_prefix_url").val().trim().length === 0 ? "private" : result,
                pda_auto_protect_new_file: $("#pda_auto_protect_new_file").prop('checked'),
                pda_gold_enable_image_hot_linking: $("#pda_gold_enable_image_hot_linking").prop('checked'),
                pda_gold_enable_directory_listing: $("#pda_gold_enable_directory_listing").prop('checked'),
                pda_prevent_access_license: $("#pda_prevent_access_license").prop('checked'),
                pda_prevent_access_version: $("#pda_prevent_access_version").prop('checked'),
                pda_gold_no_access_page: $("#pda_search_page_404_input").val(),
                whitelist_roles: $("#pda_role_select2").val(),
                whitelist_user_groups: $("#pda_uam_user_group_select2").val(),
                // pda_gold_search: $("#pda_search_page_404_input").val(),
                whitelist_roles_auto_protect: $("#pda_auto_protect_new_file_select2").val(),
                file_access_permission: $("#file_access_permission").val(),
                remove_license_and_all_data: $("#remove_license_and_all_data").prop('checked'),
                use_redirect_urls: $("#use_redirect_urls").prop('checked'),
                pda_auto_create_new_private_link: $("#pda_auto_create_new_private_link").prop('checked'),
                pda_auto_replace_protected_file: $("#pda_auto_replace_protected_file").prop('checked'),
                pda_replaced_pages_posts: $('#pda_replaced_pages_select2').val(),
                force_download : $('#pda_force_download').prop('checked'),
                pda_gold_enable_auto_activate_new_site: $('#pda_gold_enable_auto_activate_new_site').prop('checked'),
                pda_role_protection: $('#pda_role_protection').val()
            }, function(error) {
                if(error) {
                    console.error(error);
                }
                if($("#pda_prefix_url").val().trim().length === 0 ) {
                    $("#pda_prefix_url").val("private");
                }
            });
        });

        $(".custom-aws-bucket-input").each(function(key) {
            $(this).change(function() {
                $('.btn-test').show('slow');
                $('#submit').hide('slow');
            });
        });

        $('#is_aws_default').change(function(evt) {
            evt.preventDefault();
            if(this.checked) {
               $('.custom-aws-bucket-input').prop('required', false);
               $('.custom-aws-bucket').hide('slow');
                $('.btn-test').hide('slow');
                $('#submit').show('slow');
            } else {
               $('.custom-aws-bucket-input').prop('required', true);
               $('.custom-aws-bucket').show('slow');
                $('.btn-test').show('slow');
                $('#submit').hide('slow');
            }
        });

        _handleMigration();

        $('#activate-all-sites').click(function () {
            let infoSiteActivated = $('#info-site-activated');
            if (infoSiteActivated.hasClass('pda-display-none')) {
                infoSiteActivated.removeClass('pda-display-none');
            }
            let activateAllSite = $("#activate-all-sites");
            activateAllSite.attr("type", "hidden");
            $('#span-activate').append('<div id="license-loading" class="lds-ring"><div></div><div></div><div></div><div></div></div>');

            function getActivated() {
                $.ajax({
                    url: ajax_url,
                    type: 'GET',
                    data: {
                        action: 'pda_gold_activated_statistics',
                    },
                    success: function (data) {
                        if (data.hasOwnProperty('status')) {
                            siteActivated.text(data.num);
                            if (!data.status) {
                                $('#license-loading').remove();
                                activateAllSite.attr("type", "submit");
                                clearInterval(myInterval);
                                toastr.success('License activated successfully on existing sites', 'Prevent Direct Access Gold 3.0')
                            }
                        }
                    },
                    error: function (error) {
                        console.log("Errors", error);
                        $('#license-loading').remove();
                        activateAllSite.attr("type", "submit");
                        clearInterval(myInterval);
                        toastr.error('Fail to activate PDA Gold license on existing sites. Please try again.', 'Prevent Direct Access Gold 3.0')
                    },
                    timeout: 1000
                });
            }

            let siteActivated = $('#site-activated');
            let myInterval = setInterval(getActivated, 1000);

            const _data = {
                action: 'pda_gold_activate_all_sites',
                security_check: $("#prevent-direct-access-gold_nonce").val(),
            };
            $.ajax({
                url: ajax_url,
                type: 'POST',
                data: _data,
                success: function (data) {
                    if (data === 'invalid_nonce') {
                        alert('No! No! No! Verify Nonce Fails!');
                    }
                },
                error: function (error) {
                    console.log("Errors", error);
                },
                timeout: 5000
            });
        })
    });

    function _updateSettingsGeneral(settings, cb){
        var _data = {
            action: 'pda_gold_update_general_settings',
            settings: settings,
            security_check: $("#nonce_pda_v3").val(),
        };
        $('#summit').val('Submiting');
        $("#submit").prop("disabled", true);
        $.ajax({
            url: ajax_url,
            type: 'POST',
            data: _data,
            success: function(data) {
                $("#submit").prop("disabled", false);
                //Do something with the result from server
                if (data == 'invalid_nonce') {
                    alert('No! No! No! Verify Nonce Fails!');
                } else if(data) {
                    //success here
                    console.log("Success", data);
                    toastr.success('Your settings have been updated successfully!', 'Prevent Direct Access Gold 3.0')
                } else {
                    console.log("Failed", data);
                }
                cb();
            },

            error: function(error) {
                $("#submit").prop("disabled", false);
                console.log("Errors", error);
                cb(error);
            },
            timeout: 5000
        });
    }

    function _handleMigration() {
        $("#migration-form").submit(function(evt) {
            evt.preventDefault();
            var _data = {
                action: 'pda_gold_migrate_data',
                security_check: $("#nonce_pda_v3").val(),
            };
           $("#migration-progress").show('slow');
            $("#migration-progress").removeClass('hide');
            $("#migration-progress-bar").addClass("running");
            $("#submit").val("Migrating...");
            $("#submit").attr("disabled", "disabled");
            $.ajax({
               xhr: function() {
                   var xhr = new window.XMLHttpRequest();
                   xhr.upload.addEventListener("progress", function(evt) {
                       if (evt.lengthComputable) {
                           var percentComplete = evt.loaded / evt.total;
                           console.log("Percent", percentComplete);
                           $('#migration-progress-bar').css({
                               width: percentComplete * 100 + "%"
                           });
                           $('#migration-progress-bar').text(percentComplete * 100 + "% " + "Complete");
                       }
                   }, false);
                   xhr.addEventListener("progress", function (evt) {
                       if (evt.lengthComputable) {
                           var percentComplete = evt.loaded / evt.total;
                           $('#migration-progress-bar').css({
                               width: percentComplete * 100 + "%"
                           });
                           $('#migration-progress-bar').text(percentComplete * 100 + "% " + "Complete");
                       }
                   }, false);
                   return xhr;
               },
                type: 'POST',
                data: _data,
                url: ajax_url,
                success: function(data) {
                    $("#submit").val("Data migrated successfully");
                    setTimeout(function(){ location.reload(); }, 2000);
                }
           })
        });
    }

    function _handle_fully_activate() {
        $("#enable_pda_v3_form").submit(function(evt) {
            evt.preventDefault();
            _ajax_handle_fully_activate(true);
        });

        $("#enable_pda_v3_form_no_reload").submit(function(evt) {
            evt.preventDefault();
            _ajax_handle_fully_activate(false);
        });

        $("#enable_pda_v3_raw_url").submit(function(evt) {
            evt.preventDefault();
            _ajax_handle_enable_raw_url(true);
        })
    }

    function _ajax_handle_fully_activate(isReload) {
        var _data = {
            action: 'pda_gold_check_htaccess',
            security_check: $("#nonce_pda_v3").val(),
        };
        $("#enable_pda_v3").attr("disabled", "disabled");
        $.ajax({
            type: 'POST',
            data: _data,
            url: ajax_url,
            success: function (data) {
                console.log("data", data);
                if( data ) {
                    if(isReload) {
                        $("#enable_pda_v3").val("Successfully activated");
                        $("#enable_pda_v3").attr("disabled", "disabled");
                    } else {
                        $("#enable_pda_v3").attr("disabled", false);
                    }
                    toastr.success('Great! Our custom .htaccess rewrite rules are inserted correctly.', 'Prevent Direct Access Gold 3.0');
                    if(isReload) {
                        location.reload();
                    }
                } else {
                    $("#enable_pda_v3").attr("disabled", false)
                    toastr.error('Opps! Please try to update your .htaccces again with the above intructions.', 'Prevent Direct Access Gold 3.0')
                }
            }
        });
    }

    function _ajax_handle_enable_raw_url(isReload) {
        var _data = {
            action: 'pda_gold_enable_raw_url',
            security_check: $("#nonce_pda_v3").val(),
        };
        $("#enable_raw_url").attr("disabled", "disabled");
        $.ajax({
            type: 'POST',
            data: _data,
            url: ajax_url,
            success: function (data) {
                console.log("data", data);
                if( data ) {
                    if(isReload) {
                        $("#enable_raw_url").val("Successfully activated");
                        $("#enable_raw_url").attr("disabled", "disabled");
                    } else {
                        $("#enable_raw_url").attr("disabled", false);
                    }
                    toastr.success('Great! Success to fully activated the plugin.', 'Prevent Direct Access Gold 3.0');
                    if(isReload) {
                        location.reload();
                    }
                } else {
                    $("#enable_raw_url").attr("disabled", false)
                    toastr.error('Opps! Please try again or contact the plugin owner.', 'Prevent Direct Access Gold 3.0')
                }
            }
        })
    }
})(jQuery);