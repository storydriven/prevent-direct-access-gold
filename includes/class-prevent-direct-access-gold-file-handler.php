<?php
/**
 * Created by PhpStorm.
 * User: gaupoit
 * Date: 5/17/18
 * Time: 10:02
 */

if( !class_exists('Prevent_Direct_Access_Gold_File_Handler') ) {

    class Prevent_Direct_Access_Gold_File_Handler {

    	public static function move_attachment_file( $attachment_id, $metadata = [] ) {
		    $file = get_post_meta( $attachment_id, '_wp_attached_file', true );
		    $reldir = dirname( $file );
		    if ( in_array( $reldir, array( '\\', '/', '.' ), true ) )
			    $reldir = '';
            $protected_dir = path_join( Prevent_Direct_Access_Gold_File_Handler::mv_upload_dir(), $reldir );
		    return Prevent_Direct_Access_Gold_File_Handler::move_attachment_to_protected( $attachment_id, $protected_dir, $metadata );
	    }

        /**
         * Move attachment from Wordpress's folder to protected folder
         * @param $attachment_id
         * @param $protected_dir
         * @return bool
         */
        public static function move_attachment_to_protected($attachment_id, $protected_dir, $meta_input = []) {

            if( 'attachment' !== get_post_type( $attachment_id ) ) {
                return new WP_Error( 'not_attachment', sprintf(
                    __( 'The post with ID: %d is not an attachment post type.', 'prevent-direct-access-gold' ),
                    $attachment_id
                ), array( 'status' => 404) );
            }

            if( path_is_absolute( $protected_dir ) ) {
                return new WP_Error( 'protected_dir_not_relative', sprintf(
                    __( 'The new path provided: %s is absolute. The new path must be a path relative to the WP uploads directory.', 'prevent-direct-access-gold' ),
                    $protected_dir
                ), array( 'status' => 404));
            }

            $meta = empty($meta_input) ? wp_get_attachment_metadata( $attachment_id ) : $meta_input;
            $meta = is_array($meta) ? $meta : array();

            $file = get_post_meta( $attachment_id, '_wp_attached_file', true );
            $backups = get_post_meta( $attachment_id, '_wp_attachment_backup_sizes', true);

            $upload_dir = wp_upload_dir();

            $old_dir = dirname($file);
            if ( in_array( $old_dir, array( '\\', '/', '.' ), true ) ) {
                $old_dir = '';
            }

            if ( $protected_dir === $old_dir ) {
                return true;
            }

            $old_full_path = path_join( $upload_dir['basedir'], $old_dir );
            $protected_full_path =  path_join( $upload_dir['basedir'], $protected_dir );

            if ( !wp_mkdir_p( $protected_full_path ) ) {
                return new WP_Error( 'wp_mkdir_p_error', sprintf(
                    __( 'There was an error making or verifying the directory at: %s', 'prevent-direct-access-gold' ),
                    $protected_full_path
                ), array( 'status' => 500) );
            }

            //Get all files
	        $sizes = array();
	        if( array_key_exists('sizes', $meta ) ) {
		        $sizes = Prevent_Direct_Access_Gold_File_Handler::get_files_from_meta( $meta['sizes'] );
	        }
            $backup_sizes = Prevent_Direct_Access_Gold_File_Handler::get_files_from_meta( $backups );

            $old_basenames = $new_basenames = array_merge(
                array( basename( $file ) ),
                $sizes,
                $backup_sizes
            );

            $orig_basename = basename( $file );
            if( is_array( $backups ) && isset( $backups['full-orig'] ) ) {
                $orig_basename = $backups['full-orig']['file'];
            }

            $orig_filename = pathinfo( $orig_basename );
            $orig_filename = $orig_filename['filename'];

            $result = Prevent_Direct_Access_Gold_File_Handler::resolve_name_conflict( $new_basenames, $protected_full_path, $orig_filename );
            $new_basenames = $result['new_basenames'];

            Prevent_Direct_Access_Gold_File_Handler::rename_files( $old_basenames, $new_basenames, $old_full_path, $protected_full_path );

            $base_file_name = 0;

            if ( empty( $protected_dir ) ) {
            	$new_attached_file = $new_basenames[0];
            } else {
	            $new_attached_file = path_join( $protected_dir, $new_basenames[0] );
            }
            
            if( array_key_exists('file', $meta ) ) {
	            $meta['file'] = $new_attached_file;
            }
	        update_post_meta( $attachment_id, '_wp_attached_file', $new_attached_file );


	        if( $new_basenames[$base_file_name] != $old_basenames[$base_file_name] ) {
                $pattern = $result['pattern'];
                $replace = $result['replace'];
                $separator = "#";
                $orig_basename = ltrim (
                    str_replace( $pattern, $replace, $separator . $orig_basename ),
                    $separator
                );
		        $meta = Prevent_Direct_Access_Gold_File_Handler::update_meta_sizes_file($meta, $new_basenames);
                Prevent_Direct_Access_Gold_File_Handler::update_backup_files($attachment_id, $backups, $new_basenames);
            }

	        update_post_meta( $attachment_id, '_wp_attachment_metadata', $meta );
	        $guid = path_join( $protected_full_path, $orig_basename );
            wp_update_post( array( 'ID' => $attachment_id, 'guid' => $guid ) );

            return empty($meta_input) ? true : $meta;

        }

        static function mv_upload_dir( $path = '', $in_url = false ) {

            $dirpath = $in_url ? '/' : '';
            $dirpath .= '_pda';
            $dirpath .= $path;

            return $dirpath;

        }

        public static function get_files_from_meta( $input ) {
            $files = array();
            if( is_array( $input ) ) {
                foreach( $input as $size ) {
                    $files[] = $size['file'];
                }
            }
            return $files;
        }

        public static function resolve_name_conflict( $new_basenames, $protected_full_path, $orig_file_name ) {
            $conflict = true;
            $number = 1;
            $separator = "#";
            $med_filename = $orig_file_name;
            $pattern = "";
            $replace = "";
            while($conflict) {
                $conflict = false;
                foreach( $new_basenames as $basename ) {
                    if( is_file ( path_join( $protected_full_path, $basename ) ) ) {
                        $conflict = true;
                        break;
                    }
                }

                if($conflict) {
                    $new_filename = "$orig_file_name-$number";
                    $number++;
                    $pattern = "$separator$med_filename";
                    $replace = "$separator$new_filename";
                    $new_basenames = explode(
                        $separator,
                        ltrim(
                            str_replace( $pattern, $replace, $separator . implode( $separator, $new_basenames ) ),
                            $separator
                        )
                    );

                }
            }


            return array(
                'new_basenames' => $new_basenames,
                'pattern' => $pattern,
                'replace' => $replace
            );

        }

        public static function rename_files($old_basenames, $new_basenames, $old_dir, $protected_dir) {
            $unique_old_basenames = array_values( array_unique( $old_basenames ) );
            $unique_new_basenames = array_values( array_unique( $new_basenames ) );
            $i = count( $unique_old_basenames );
            while( $i-- ) {
                $old_fullpath = path_join( $old_dir, $unique_old_basenames[$i] );
                $new_fullpath = path_join( $protected_dir, $unique_new_basenames[$i] );
                if( is_file( $old_fullpath ) ) {
                    rename( $old_fullpath, $new_fullpath );

                    if( !is_file($new_fullpath) ) {
                        return new WP_Error(
                            'rename_failed',
                            sprintf(
                                __( 'Rename failed when trying to move file from: %s, to: %s', 'prevent-direct-access-gold' ),
                                $old_fullpath,
                                $new_fullpath
                            )
                        );
                    }
                }

            }
        }

        public static function update_meta_sizes_file($meta, $new_basenames) {
            if ( array_key_exists( 'sizes', $meta ) && is_array( $meta['sizes'] ) ) {
                $i = 0;

                foreach ( $meta['sizes'] as $size => $data ) {
                    $meta['sizes'][$size]['file'] = $new_basenames[++$i];
                }
                error_log("Metadata");
                error_log(serialize($meta));
            }
            return $meta;
        }

        public static function update_backup_files($attachment_id, $backups, $new_basenames) {
            if ( is_array( $backups ) ) {
                $i = 0;
                $l = count( $backups );
                $new_backup_sizes = array_slice( $new_basenames, -$l, $l );

                foreach ( $backups as $size => $data ) {
                    $backups[$size]['file'] = $new_backup_sizes[$i++];
                }
                update_post_meta( $attachment_id, '_wp_attachment_backup_sizes', $backups );
            }
        }

	    /**
	     * Get attachment id from url
	     * @param $file_info
	     *
	     * @return int > -1 if found attachment
	     */
        public static function get_attachment_id_from_url( $file_info ) {
        	global $wpdb;
        	$basename = $file_info['basename'];
        	$prepare = $wpdb->prepare(
         "SELECT post_id, meta_value
                 FROM $wpdb->postmeta
                 WHERE meta_key = %s
                 	  AND meta_value LIKE %s
        		",
		        "_wp_attachment_metadata",
                "%$basename%"
	        );
        	$attachments = $wpdb->get_results( $prepare, ARRAY_A );
        	$attachment_id = -1;
        	foreach ($attachments as $attachment) {
        		$meta_value = unserialize( $attachment['meta_value'] );
        		if ( isset( $meta_value['file']) ) {
			        if( ltrim( dirname( $meta_value['file'] ), '/') === ltrim($file_info['dirname'], '/') ) {
				        $attachment_id = $attachment['post_id'];
				        break;
			        }
		        }
		        
	        }
	        return $attachment_id;
        }
	    /**
	     * @param $file_path
	     */
        public static function get_attachment_id_from_file_path( $file_path ) {
	        global $wpdb;

	        $path = ltrim( $file_path, '/');
	        $prepare = $wpdb->prepare(
		        "SELECT post_id
                 FROM $wpdb->postmeta
                 WHERE meta_key = %s
                 	  AND meta_value = %s",
		        "_wp_attached_file",
		        "$path"
	        );
	        $attachment = $wpdb->get_row( $prepare );
	        if( is_null( $attachment) ) {
	        	return -1;
	        }

	        return $attachment->post_id;
        }
    }
}