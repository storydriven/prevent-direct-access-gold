<?php
/**
 * Created by PhpStorm.
 * User: gaupoit
 * Date: 5/18/18
 * Time: 09:39
 */

if( !class_exists('Prevent_Direct_Access_Gold_Htaccess') ) {

    class Prevent_Direct_Access_Gold_Htaccess
    {

        static function get_the_rewrite_rules() {
            $upload = wp_upload_dir();
            $baseurl = $upload['baseurl'];
            if ( is_ssl() ) {
                $baseurl = str_replace( 'http://', 'https://', $baseurl );
            }
            $upload_path = str_replace(site_url('/'), '', $baseurl);
	        $secret_query_string = PDA_v3_Constants::$secret_param;
	        $multisite_cond = "";
	        $subdomain_cond = "(?:[_0-9a-zA-Z-]+/)?";


            if( is_multisite() ) {
	            if( is_main_site() ) {
		            $upload_path .= '/sites/1';
	            }
	            $upload_path = preg_replace('/\/sites\/[0-9]+/', '(?:/sites/[0-9]+)?', $upload_path);
	            $multisite_cond = '(?:[_0-9a-zA-Z-]+/)?';
            }


            $is_multisite_and_sub_directory_mode = is_multisite() && is_subdomain_install();
            if( $is_multisite_and_sub_directory_mode ) {
            	$upload_path = '(?:[_0-9a-zA-Z-]+/)?' . $upload_path;
            }

	        $pattern = Prevent_Direct_Access_Gold_File_Handler::mv_upload_dir('/.*\.\w+)$', true );

            $old_protected_path = "$upload_path($pattern";

	        $private_link_rules =  Prevent_Direct_Access_Gold_Htaccess::generate_private_link_rule( $secret_query_string, $multisite_cond, $subdomain_cond );
			$original_rules = array(
				'RewriteCond %{HTTP_USER_AGENT} !facebookexternalhit/[0-9]',
				'RewriteCond %{HTTP_USER_AGENT} !Twitterbot/[0-9]',
				'RewriteCond %{HTTP_USER_AGENT} !Googlebot/[0-9]',
				'RewriteRule ^' . $old_protected_path . " index.php?$secret_query_string=$1 [QSA,L]",
				'# Prevent Direct Access Rewrite Rules End'
			);
			$hotlinking_rules = Prevent_Direct_Access_Gold_Htaccess::generate_hot_linking_rules();
			$ip_black_list_rules = Prevent_Direct_Access_Gold_Htaccess::generate_ip_black_list_rules();
			$ip_white_list_rules = Prevent_Direct_Access_Gold_Htaccess::generate_ip_white_list_rules();
			$ip_white_list_website_rules = Prevent_Direct_Access_Gold_Htaccess::generate_ip_white_list_website_rules();
            $setting = new Pda_Gold_Functions;
            $readme_licensed = [];
						if($setting->get_site_settings(PDA_v3_Constants::PDA_PREVENT_ACCESS_LICENSE)) {
                $readme_licensed = [
                    PHP_EOL."<files readme.html>",
                    "order allow,deny",
                    "deny from all",
                    "</files>".PHP_EOL,

                    "<files license.txt>",
                    "order allow,deny",
                    "deny from all",
                    "</files>".PHP_EOL,

                    "<files wp-config-sample.php>",
                    "order allow,deny",
                    "deny from all",
                    "</files>",
                ];
            }

	        $rewrite_rules = array_merge(
                $ip_white_list_website_rules,
                $ip_black_list_rules,
                $ip_white_list_rules,
		        $hotlinking_rules,
		        array(
		        '# Prevent Direct Access Rewrite Rules',
		        $private_link_rules,
	            ),
		        $original_rules,
                $readme_licensed

	        );

	        //user have to manually add the rules to .htaccess
			$is_permalinks_not_enabled = !is_multisite() && !get_option('permalink_structure');
	        if( $is_permalinks_not_enabled ) {
		        $home_root = parse_url( home_url() );
		        if ( isset( $home_root['path'] ) )
			        $home_root = trailingslashit( $home_root['path'] );
		        else
			        $home_root = '/';

		        array_splice( $rewrite_rules, 1, 0, array(
			        '<ifModule mod_rewrite.c>',
			        'RewriteEngine On',
			        'RewriteBase ' . $home_root
		        ) );
		        array_splice( $rewrite_rules, -1, 0, array(
			        '</ifModule>'
		        ) );
			}
			
	        return apply_filters(PDA_v3_Constants::$hooks['HTACCESS'], $rewrite_rules);
        }

        static function register_rewrite_rules() {
	        if(Prevent_Direct_Access_Gold_Htaccess::is_htaccess_writeable()) {
		        add_filter( 'mod_rewrite_rules', 'Prevent_Direct_Access_Gold_Htaccess::pda_handle_htaccess_rewrite_rules', 9999, 2);
		        flush_rewrite_rules();
				return true;
	        }
	        return false;
        }

	     static function pda_handle_htaccess_rewrite_rules( $rules ) {
		    $pattern = "RewriteRule ^index\.php$ - [L]\n";

		    $option_index = Prevent_Direct_Access_Gold_Htaccess::add_option_indexes_rule($rules);
		    $pda_rules = Prevent_Direct_Access_Gold_Htaccess::get_the_rewrite_rules();

		    return str_replace( $pattern, "$pattern\n" . implode( "\n", $pda_rules ) . "\n\n", $rules. $option_index );
	    }

	    static function add_option_indexes_rule( $rules ) {
		    $directory = new Pda_Gold_Functions;
		    $pda_gold_enable_directory_listing = $directory->get_site_settings('pda_gold_enable_directory_listing') === true;
		    $option_index = strpos($rules, "Options -Indexes") === false && $pda_gold_enable_directory_listing ? "Options -Indexes" : '';
		    return $option_index;
	    }

        static function is_htaccess_writeable() {
	        global $is_apache;
	        return $is_apache
	               && !is_multisite()
	               && get_option('permalink_structure')
	               && is_writable( get_home_path() . '.htaccess');
        }

        private static function generate_private_link_rule( $secret_query_string, $multi_cond = "" ) {
			$settings = new Pda_Gold_Functions();
			$private_param = PDA_v3_Constants::$secret_private_link;
	        $downloadFileRedirect =
		        'index.php' . "?$secret_query_string=$1&$private_param [L]";
        	$prefix = $settings->prefix_roles_name(PDA_v3_Constants::PDA_PREFIX_URL);
        	return "RewriteRule ^$multi_cond" . "$prefix/([a-zA-Z0-9-_.]+)$ $downloadFileRedirect";
		}

		private static function generate_hot_linking_rules() {
			$setting = new Pda_Gold_Functions;
			$site_settings = get_site_option(PDA_v3_Constants::SITE_OPTION_NAME);
            $rules = array();
			if($site_settings) {
			    $site_options = unserialize($site_settings);
                if(array_key_exists(PDA_v3_Constants::PDA_GOLD_ENABLE_IMAGE_HOT_LINKING ,$site_options)) {
                    $is_enable_hot_linking = $site_options[PDA_v3_Constants::PDA_GOLD_ENABLE_IMAGE_HOT_LINKING];
                    if( $is_enable_hot_linking == "true" ) {
                        $domain_info = $setting->get_domain_info();
//                        $host = $domain_info['host'];
                        $rules = array(
                            '# Prevent Direct Access Prevent Hotlinking Rules',
                            'RewriteCond %{HTTP_REFERER} !^$',
                            "RewriteCond %{HTTP_REFERER} !^$domain_info [NC]",
                            'RewriteRule \.(gif|jpg|jpeg|bmp|zip|rar|mp3|flv|swf|xml|png|css|pdf)$ - [F]',
                            '# Prevent Direct Access Prevent Hotlinking Rules End',
                            ''
                        );
                    }
                }
            }
            return $rules;
		}

        private static function generate_ip_black_list_rules() {
            $str_ip_lock = get_option('pda_gold_ip_block');
            $arr_ip_lock = explode(";", $str_ip_lock);
            $rules = [];
            if ($arr_ip_lock[0] != null) {
                array_push($rules, '# Prevent Direct Access IP Blacklist Rules');
                $ip = ['*.*.*.*','*.*.*','*.*','*'];
                for ($i = 0; $i < count($arr_ip_lock); $i++) {
                    array_push($rules, "RewriteCond %{REMOTE_ADDR} !^".str_replace($ip, '', $arr_ip_lock[$i]));
                }
                array_push($rules, '# Prevent Direct Access IP Blacklist Rules End'.PHP_EOL);
            }
            return $rules;
        }

        public static function generate_ip_white_list_rules () {
            $default = [];
            return apply_filters('pdav3_ip_white_list', $default) ;
        }

        public static function generate_ip_white_list_website_rules () {
            $default = [];
            return apply_filters('pdav3_ip_white_list_website', $default) ;
        }

        public static function check_rewrite_rules() {

        	$upload_dir = wp_upload_dir();
        	$secret_param_test = PDA_v3_Constants::$secret_param_test;


			$file_name = PDA_v3_Constants::$pda_test_file_name;

        	$protected_url = Prevent_Direct_Access_Gold_File_Handler::mv_upload_dir("/$file_name?$secret_param_test=1", true );

        	$checks = array(
        		$upload_dir['baseurl'] . $protected_url
	        );

        	$checks = apply_filters( 'pda_v3_rewrite_rules_check_urls', $checks );

        	error_log("Checks: " . $checks[0]);

        	$passed = true;

        	foreach( $checks as $url ) {

        		$check = wp_remote_get( $url );

        		if ( is_wp_error( $check )
		            || ! isset( $check['response']['code'] ) || 200 != $check['response']['code']
		            || ! isset( $check['body'] ) || 'pass' != $check['body'] ) {
        			$passed = false;
		        } else {
        			$passed = true;
		        }

		        error_log($url);
		        error_log(serialize($check['response']['code']));

		        if($passed) {
        			break;
		        }
	        }

	        error_log("Checking htaccess: $passed");

	        return $passed;
        }

        static function get_nginx_rules() {
	        $upload = wp_upload_dir();
	        $upload_path = str_replace(site_url('/'), '', $upload['baseurl']);
	        $secret_query_string = PDA_v3_Constants::$secret_param;

	        $pattern = Prevent_Direct_Access_Gold_File_Handler::mv_upload_dir('/.*\.\w+)$', true );
	        $old_protected_path = "$upload_path($pattern";

	        $original_rules = array(
	        	"rewrite $old_protected_path \"/index.php?$secret_query_string=$1\" last;"
	        );

	        $settings = new Pda_Gold_Functions();
	        $private_param = PDA_v3_Constants::$secret_private_link;
	        $downloadFileRedirect = "/index.php?$secret_query_string=$1&$private_param";
	        $prefix = $settings->prefix_roles_name(PDA_v3_Constants::PDA_PREFIX_URL);

	        $private_link_rules = array(
	            "rewrite $prefix/([a-zA-Z0-9-_.]+)$ \"$downloadFileRedirect\" last;"
	        );

	        $rewrite_rules = array_merge(
	        	$original_rules,
		        $private_link_rules
	        );

	        return apply_filters( PDA_v3_Constants::$hooks['NGINX'], $rewrite_rules );

        }
	
	    static function get_iis_rules() {
		    $upload = wp_upload_dir();
		    $upload_path = str_replace(site_url('/'), '', $upload['baseurl']);
		    $secret_query_string = PDA_v3_Constants::$secret_param;
		
		    $pattern = Prevent_Direct_Access_Gold_File_Handler::mv_upload_dir('/.*\.\w+)$', true );
		    $old_protected_path = "$upload_path($pattern";
		
		    $settings = new Pda_Gold_Functions();
		    $private_param = PDA_v3_Constants::$secret_private_link;
		    $downloadFileRedirect = "/index.php?$secret_query_string=$1&$private_param";
		    $prefix = $settings->prefix_roles_name(PDA_v3_Constants::PDA_PREFIX_URL);
		
		    return array_merge(
			    self::render_iss_template_rule( 'pda-original-link', $old_protected_path, "/index.php?$secret_query_string={R:1}"),
			    self::render_iss_template_rule( 'pda-private-link', "$prefix/([a-zA-Z0-9-_]+)$", $downloadFileRedirect)
		    );
	    }
	    
	    private static function render_iss_template_rule( $rule_name, $pattern, $redirect_url ) {
	        return array(
	            "<rule name=\"$rule_name\" patternSyntax=\"ECMAScript\">",
	            "\tmatch url=\"$pattern\" />",
	            "\t\t<conditions logicalGrouping=\"MatchAll\" trackAllCaptures=\"false\">",
	            "\t\t\t<add input=\"{REQUEST_FILENAME}\" matchType=\"IsDirectory\" negate=\"true\" />",
	            "\t\t</conditions>",
	            "\t<action type=\"Rewrite\" url=\"$redirect_url\" />",
	            "</rule>"
	        );
	    }
	}

}