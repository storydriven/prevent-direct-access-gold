<?php
/**
 * User: gaupoit
 * Date: 6/26/18
 * Time: 16:24
 *
 * @package pda_services
 */

if ( ! class_exists( 'PDA_Services' ) ) {
    /**
     * PDA services that containing the functions to interact with media files.
     *
     * Class PDA_Services
     */
    class PDA_Services {
        /**
         * Get post by it's permanent url.
         *
         * @param string $url Permanent url.
         *
         * @return array Posts result.
         */
        public static function get_post_by_url( $url ) {
            $wp_upload_dir  = wp_upload_dir();
            $baseurl        = $wp_upload_dir['baseurl'];
            $meta_value     = str_replace( $baseurl . '/', '', $url );
            $arr_meta_value = explode( '/', $meta_value );
            if ( ! in_array( '_pda', $arr_meta_value, true ) ) {
                array_unshift( $arr_meta_value, '_pda' );
            }
            $meta_value = implode( '/', $arr_meta_value );
            $attachment = get_posts( array(
                'post_type'  => 'attachment',
                'meta_key'   => '_wp_attached_file',
                'meta_value' => $meta_value,
            ) );

            return $attachment;
        }

        /**
         * Generate a new private link for the protected medial file with its existed private link.
         *
         * @param string $url The private url.
         * @param string $post_fix (optional) Post fix string for the unique private uri.
         * For example, if post_fix is -pda then the private uri should be <random_unique_string>-pda.
         *
         * @return string Return the private url if the media file is protected, otherwise returning the original link.
         */
        public static function generate_private_link( $url, $post_fix ) {
            $repo        = new PDA_v3_Gold_Repository();
            $prefix      = new Pda_Gold_Functions();
            $prefix_name = $prefix->prefix_roles_name( PDA_v3_Constants::PDA_PREFIX_URL );
            $private_uri = str_replace( home_url() . '/' . $prefix_name . '/', '', $url );
            $post        = $repo->get_post_id_by_private_uri( $private_uri );
            $data        = array(
                'id'       => $post->post_id,
                'post_fix' => $post_fix,
            );

            return $repo->generateExpired( $data );
        }

        /**
         * Checking the success of creating a new private link
         *
         * @param array $data Input data including post id, is_prevented, limit downloads and expired date.
         *
         * @return bool Creation's result.
         */
        public function check_before_create_private_link( $data ) {
            $repo = new PDA_v3_Gold_Repository();
            if ( ! $repo->is_protected_file( $data['id'] ) ) {
                return new WP_Error( 'post_not_found', sprintf(
                    __( 'Cannot found the post : %s', 'prevent-direct-access-gold' ),
                    $data['id']
                ), array( 'status' => 404 ) );
            }
            $url = isset( $data['url'] ) ? $data['url'] : Pda_v3_Gold_Helper::generate_unique_string();
            if ( ! is_null( $repo->get_advance_file_by_url( $url ) ) ) {
                return new WP_Error( 'duplicate_url', sprintf(
                    __( 'This url : %s already existed!', 'prevent-direct-access-gold' ),
                    $url
                ), array( 'status' => 400 ) );
            }
            $result = $repo->create_private_link( array(
                'post_id'         => $data['id'],
                'is_prevented'    => isset( $data['is_prevented'] ) ? $data['is_prevented'] : true,
                'limit_downloads' => isset( $data['limit_downloads'] ) ? $data['limit_downloads'] : null,
                'expired_date'    => isset( $data['expired_days'] ) ? $this->get_expired_time_stamp( $data['expired_days'] ) : null,
                'url'             => $url,
            ) );

            return $result > 0;
        }

        /**
         * Get timestamp of expired date
         *
         * @param string $days_to_expired The number of days will be expired from now.
         *
         * @return int The timestamp.
         */
        function get_expired_time_stamp( $days_to_expired ) {
            $curr_date    = new DateTime();
            $expired_date = $curr_date->modify( $days_to_expired . ' day' );

            return $expired_date->getTimestamp();
        }

        /**
         * Auto create a new private link
         *
         * @param mixed $data New private link's information.
         */
        public function auto_create_new_private_link( $data ) {
            $repo             = new PDA_v3_Gold_Repository();
            $all_private_link = $repo->get_all_private_links( $data['id'] );
            if ( empty( $all_private_link ) ) {
                $this->check_before_create_private_link( $data );
            } else {
                $is_prevented = false;
                foreach ( $all_private_link as $private_link ) {
                    $limit_downloads = ( is_null( $private_link['limit_downloads'] ) || $private_link['hits_count'] < $private_link['limit_downloads'] ) ? true : false;
                    $curr_date       = new DateTime();
                    $expired_date    = ( is_null( $private_link['expired_date'] ) || $private_link['expired_date'] > $curr_date->getTimestamp() ) ? true : false;
                    if ( '1' === $private_link['is_prevented'] && $limit_downloads && $expired_date ) {
                        $is_prevented = true;
                        break;
                    }
                }
                if ( ! $is_prevented ) {
                    $this->check_before_create_private_link( $data );
                }
            }
        }

        public function check_before_create_private_link_for_magic_link( $data ) {
            $repo = new PDA_v3_Gold_Repository();
            if ( ! $repo->is_protected_file( $data['post_id'] ) ) {
                return new WP_Error( 'post_not_found', sprintf(
                    __( 'Cannot found the post : %s', 'prevent-direct-access-gold' ),
                    $data['post_id']
                ), array( 'status' => 404 ) );
            }
            $url = Pda_v3_Gold_Helper::generate_unique_string();
            if ( ! is_null( $repo->get_advance_file_by_url( $url ) ) ) {
                return new WP_Error( 'duplicate_url', sprintf(
                    __( 'This url : %s already existed!', 'prevent-direct-access-gold' ),
                    $url
                ), array( 'status' => 400 ) );
            }

            $func      = new Pda_Gold_Functions;
            $is_synced = $func->check_file_synced_s3( $data['post_id'] );

            $private_link_user = $repo->get_private_link_for_user_by_post_id( $data['post_id'] );

            if ( $private_link_user === null ) {
                $repo->create_private_link( array(
                    'post_id'         => $data['post_id'],
                    'is_prevented'    => true,
                    'limit_downloads' => isset( $data['private_link_user_limit_downloads'] ) ? $data['private_link_user_limit_downloads'] : null,
                    'expired_date'    => isset( $data['private_link_user_expired_days'] ) ? $this->get_expired_time_stamp( $data['private_link_user_expired_days'] ) : null,
                    'url'             => $url,
                    'roles'           => isset( $data['selectedRoles'] ) ? $data['selectedRoles'] : "",
                    'type'            => $is_synced ? PDA_v3_Constants::PDA_PRIVATE_LINK_S3_USER : PDA_v3_Constants::PDA_PRIVATE_LINK_USER,
                ) );
            } else {
	            $time = current_time( $private_link_user->time );
                $date = new DateTime( $time );
                $expired_date = isset( $data['private_link_user_expired_days'] ) ? $date->modify( $data['private_link_user_expired_days'] . ' day' ) : null;
                $repo->update_private_link( $private_link_user->ID, array(
                    'limit_downloads' => isset( $data['private_link_user_limit_downloads'] ) ? $data['private_link_user_limit_downloads'] : null,
                    'expired_date'    => $expired_date !== null ? $expired_date->getTimestamp() : $expired_date,
                    'roles'           => isset( $data['selectedRoles'] ) ? $data['selectedRoles'] : "",
                    'type'            => $is_synced ? 'p_user_s3' : 'p_user',
                ) );
            }
        }

        public function handle_data_for_get_private_link_magic_link( $data ) {
            $repo              = new PDA_v3_Gold_Repository();
            $private_link_user = $repo->get_private_link_for_user_by_post_id( $data['post_id'] );
            if ( $private_link_user !== null ) {
                $days = "";
                if ( $private_link_user->expired_date !== null ) {
                    $expire_day = date( 'Y-m-d', $private_link_user->expired_date );
                    $create_day = date_format( date_create( $private_link_user->time ), 'Y-m-d' );
                    $date1      = new DateTime( $expire_day );
                    $date2      = new DateTime( $create_day );
                    $result     = $date1->diff( $date2 );
                    $days       = $result->days;
                }
                $full_url = Pda_v3_Gold_Helper::get_private_url( $private_link_user->url );
                return array(
                    'ID'              => $private_link_user->ID,
                    'post_id'         => $private_link_user->post_id,
                    'limit_downloads' => $private_link_user->limit_downloads,
                    'roles'           => $private_link_user->roles,
                    'expired_date'    => $days,
                    'full_url'        => $full_url,
                    'is_prevented'    => $private_link_user->is_prevented,
                    'time'            => strtotime( $private_link_user->time ),
                );
            }
        }
	
	    /**
	     * Find and replace the protected file.
	     *
	     * @param $content
	     */
        public function find_and_replace_protected_file( $content ) {
	        $img = array();
	        preg_match_all( '/<img\s[^>]*?src\s*=\s*[\'\"]([^\'\"]*?)[\'\"][^>]*?>/iU', $content, $img );
	        $links_file = array_unique( $img[1] );
	
	        $elements = array();
	        preg_match_all( '/<a(.*)href=\"([^\"]*)\"(.*)>(.*)<\/a>/iU', $content, $elements );
	        $links_href = array_unique( $elements[2] );
	        $home_url = rtrim( Pda_v3_Gold_Helper::get_home_url_with_ssl(), '/' );
            $links_href = array_map( function($value) use($home_url, &$content) {
	            if ( strpos($value,  $home_url) === false ) {
                    $content = str_replace( 'href="' . $value . '"', 'href="'. $home_url . $value . '"', $content);
                    return $home_url . $value;
                }
	            return $value;
            }, $links_href);
	        $files = array_unique ( array_merge( $links_file, $links_href ) );
	        $wp_upload_dir = wp_upload_dir();
	        $baseurl       = $wp_upload_dir['baseurl'];
	        foreach ( $files as $file ) {
		        preg_match( '(-\d+x\d+)', $file, $matches );
		        if ( empty( $matches ) ) {
			        $url_file = $file;
		        } else {
			        $url_file = str_replace( $matches[0], "", $file );
		        }
		        
		
		        $meta_value     = str_replace( $baseurl . '/', "", $url_file );
		        $attachment = get_posts( array(
			        'post_type'  => 'attachment',
			        'meta_key'   => '_wp_attached_file',
			        'meta_value' => $meta_value
		        ) );
		        
		        if ( empty( $attachment ) ) {
			        $meta_value = '_pda/' . $meta_value;
			        $attachment = get_posts( array(
				        'post_type'  => 'attachment',
				        'meta_key'   => '_wp_attached_file',
				        'meta_value' => $meta_value
			        ) );
		        }
		        
		        if ( ! empty( $attachment ) ) {
		        	$new_file = $baseurl . '/' . $meta_value;
		            $content = str_replace( $file, $new_file, $content);
		        }
	        }
	        
	        return $content;
        }

	
	    /**
	     * Handle license info by get license information from API and save it to database.
	     *
	     */
        public function handle_license_info() {
	        $license_info = YME_LICENSE::getLicenseInfo( PDA_v3_Constants::LICENSE_KEY );
	        if ( ! isset( $license_info->expired_date ) ) {
	        	$license = get_option( PDA_v3_Constants::LICENSE_KEY );
	        	YME_LICENSE::checkExpiredLicense( $license );
		        $license_info = YME_LICENSE::getLicenseInfo( PDA_v3_Constants::LICENSE_KEY );
	        }
	        update_option( PDA_v3_Constants::LICENSE_INFO, base64_encode ( wp_json_encode( $license_info ) ), 'no' );
	        return $license_info;
        }
	
	    public function debug_handle_license_info( $license ) {
        	update_option( PDA_v3_Constants::LICENSE_KEY, $license, 'no' );
		    $license_info = YME_LICENSE::getLicenseInfo( $license );
		    if ( ! isset( $license_info->expired_date ) ) {
			    $license = get_option( PDA_v3_Constants::LICENSE_KEY );
			    YME_LICENSE::checkExpiredLicense( $license );
			    $license_info = YME_LICENSE::getLicenseInfo( PDA_v3_Constants::LICENSE_KEY );
		    }
		    update_option( PDA_v3_Constants::LICENSE_INFO, base64_encode ( wp_json_encode( $license_info ) ), 'no' );
		    $cronjob_handler = new PDA_Cronjob_Handler();
		    $cronjob_handler->schedule_ls_cron_job( true );
		    return $license_info;
	    }
	
	    /**
	     * Get license information.
	     *
	     * @return bool|mixed
	     */
        public function get_license_info() {
        	$license_info = get_option( PDA_v3_Constants::LICENSE_INFO );
        	error_log( wp_json_encode( $license_info ) );
        	if ( ! $license_info ) {
        		return false;
	        }
        	return json_decode( base64_decode( get_option( PDA_v3_Constants::LICENSE_INFO ) ) );
        }

        /**
         * Set default setting for File Access Permission
         */
        public function pda_gold_set_default_setting_for_fap() {
            $settings = get_option( PDA_v3_Constants::OPTION_NAME );
            if ( ! $settings ) {
                $pda_default_fap = array(
                    PDA_v3_Constants::FILE_ACCESS_PERMISSION => "admin_users",
                );
                update_option( PDA_v3_Constants::OPTION_NAME, serialize( $pda_default_fap ) );
            }
        }
	
	    public function pda_gold_set_default_setting_for_role_protection() {
        	$settings = get_option( PDA_v3_Constants::OPTION_NAME, false );
        	if ( false === $settings ) {
        		$pda_default_role_protection = array(
			        PDA_v3_Constants::PDA_GOLD_ROLE_PROTECTION => array( 'author', 'editor' ),
		        );
        		update_option( PDA_v3_Constants::OPTION_NAME, serialize( $pda_default_role_protection ) );
        	} else {
        		$options = unserialize( $settings );
        		if ( ! array_key_exists( PDA_v3_Constants::PDA_GOLD_ROLE_PROTECTION, $options ) ) {
        			$options[PDA_v3_Constants::PDA_GOLD_ROLE_PROTECTION] = array( 'author', 'editor' );
        			update_option( PDA_v3_Constants::OPTION_NAME, serialize( $options ) );
        		}
        	}
        }

        public function handle_membership_for_fap( $post_id ) {
	        $pda_admin_membership = new Pda_Membership_Integration_Admin( "", "" );
        	if ( $pda_admin_membership->do_not_have_any_membership_plugins() ) {
		        if ( class_exists( 'Pda_Membership_Notification' ) ) {
			        $notification = new Pda_Membership_Notification();
			        return $notification->render_notification();
		        }
	        } else {
		        global $userAccessManager;
		        $all_memberships      = "";
		
		        if ( class_exists( 'MS_Model_Membership' ) ) {
			        $member = MS_Factory::load( 'MS_Model_Membership', [] );
			        $memberships = $member->get_membership_names();
			        $all_memberships .= $this->render_ui_for_all_membership( $memberships, $post_id, 'ppwp_select_membership_2', 'Membership 2', 'memberships_2' );
		        }
		        if ( is_object( $userAccessManager ) ) {
			        $user_groups = $pda_admin_membership->get_uam_user_groups();
			        $all_memberships .= $this->render_ui_for_all_membership( $user_groups, $post_id, 'ppwp_select_uam', 'User Access Manager', 'user_access_manager' );
		        }
		        if ( function_exists( 'pmpro_hasMembershipLevel' ) ) {
			        $paid_memberships = $pda_admin_membership->get_data_memberships_pro();
			        $all_memberships .= $this->render_ui_for_all_membership( $paid_memberships, $post_id, 'ppwp_paid_memberships', 'Paid Membership Pro', 'paid_memberships_pro' );
		        }
		        if ( function_exists( 'wc_memberships_is_user_member' ) ) {
			        $woo_memberships = wc_memberships_get_membership_plans();
			        $all_memberships .= $this->render_ui_for_all_membership( $woo_memberships, $post_id, 'ppwp_woo_memberships', 'WooCommerce Memberships', 'woo_memberships' );
		        }
		        if ( class_exists( "WC_Subscriptions_Admin" ) ) {
			        $woo_subcriber = $pda_admin_membership->get_all_items_in_woo_subcriptions();
			        $all_memberships .= $this->render_ui_for_all_membership( $woo_subcriber, $post_id, 'ppwp_woo_subscriptions', 'WooCommerce Subscriptions', 'woo_subscriptions' );
		        }
		
		        return $all_memberships;
	        }
        }

        function render_ui_for_all_membership( $all_item_members, $post_id, $id, $name, $key ) {
            $members         = get_post_meta( $post_id, PDA_v3_Constants::$pda_meta_key_memberships_integration, true );
            $member_selected = array_key_exists( $key, (array)$members ) ? $members[$key] : "";

            $options = array_map(function ( $member ) use ( $member_selected, $key ) {
                if ( $key === "woo_subscriptions" ) {
                    $selected = in_array( $member, explode( ";", $member_selected ) ) ? "selected" : "";
                    return "<option $selected value='" . $member . "'>" . wc_get_product( $member )->get_title() . "</option>";
                }
                if ( $key === "paid_memberships_pro" || $key === "woo_memberships" ) {
                    $selected = in_array( $member->name, explode( ";", $member_selected ) ) ? "selected" : "";
                    return "<option $selected value='" . $member->name . "'>" . $member->name . "</option>";
                }
                if ( $key === "user_access_manager" ) {
                    $selected = in_array( $member['name'], explode( ";", $member_selected ) ) ? "selected" : "";
                    return "<option $selected value='" . $member['name'] . "'>" . $member['name'] . "</option>";
                }
                if ( $key === "memberships_2" ) {
                    $selected = in_array( $member, explode( ";", $member_selected ) ) ? "selected" : "";
                    return "<option $selected value='$member'>$member</option>";
                }

            }, $all_item_members );

            $options = implode( "", $options );

            return "<div>
                        <label class='ppwp_label_for_membership'>$name</label>
                        <select id='$id' class='ppwp_select2_for_membeships' multiple>
                            $options
                        </select>
                    </div>";
        }
    }
}