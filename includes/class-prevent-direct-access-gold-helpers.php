<?php
/**
 * Created by PhpStorm.
 * User: gaupoit
 * Date: 5/18/18
 * Time: 15:08
 */

if ( ! class_exists( 'Pda_v3_Gold_Helper' ) ) {

	class Pda_v3_Gold_Helper {

		public static function generate_unique_string( $post_fix = '' ) {
			return uniqid() . $post_fix;
		}

		public static function get_plugin_configs() {
			return array( 'custom' => 'custom_v3_181191' );
		}

		public static function get_guid( $file_name, $request_url, $file_type ) {
			$guid = preg_replace( "/-\d+x\d+.$file_type$/", ".$file_type", $request_url );
		}

		public static function get_private_url( $uri ) {
			$prefix      = new Pda_Gold_Functions();
			$prefix_name = $prefix->prefix_roles_name( PDA_v3_Constants::PDA_PREFIX_URL );
			$setting     = new Pda_Gold_Functions;
			if ( $setting->get_site_settings( PDA_v3_Constants::USE_REDIRECT_URLS ) ) {
				return home_url( '/' ) . "index.php?" . PDA_v3_Constants::$secret_param . "=" . $uri . "&pdav3_rexypo=ymerexy";
			} else {
				return home_url( '/' ) . $prefix_name . "/$uri";
			}


		}

		public static function is_pdf( $mime_type ) {
			return $mime_type == "application/pdf";
		}

		public static function is_video( $mime_type ) {
			return strstr( $mime_type, "video/" );
		}

		public static function is_audio( $mime_type ) {
			return strstr( $mime_type, "audio/" );
		}

		public static function is_image( $file, $mime_type ) {
			if ( function_exists( 'exif_imagetype' ) ) {
				return exif_imagetype( $file );
			} else {
				return strstr( $mime_type, "image/" );
			}
		}

		public static function is_html( $mime_type ) {
			return $mime_type == "text/html";
		}

		public static function is_migrated_data_from_v2() {
			return Pda_Gold_Functions::is_data_migrated();
		}

		public static function only_track_http_method( $request_method ) {
			return "get" === strtolower( $request_method );
		}

		public static function get_expired_time_stamp( $days_to_expired ) {
			$curr_date    = new DateTime();
			$expired_date = $curr_date->modify( $days_to_expired . ' day' );

			return $expired_date->getTimestamp();
		}

		public static function timestamp_to_local_date( $timestamp ) {
			$date_format = get_option( 'date_format' );
			$time_format = get_option( 'time_format' );

			return get_date_from_gmt( date( 'Y-m-d H:i:s', $timestamp ), "$date_format $time_format" );
		}

		public static function map_addons_id( $addons ) {
			$massaged_addons = array_filter( array_map( function ( $addon ) {
				return self::addons( $addon );
			}, explode( ';', $addons ) ), function ( $item ) {
				return ! empty( $item );
			} );

			return implode( ', ', $massaged_addons );
		}

		public static function addons( $id ) {
			switch ( $id ) {
				case '77803382' :
					return 'PDA Protect WordPress Videos';
				case '77806417':
					return 'PDA Private Magic Links';
				case '77805157':
					return 'PDA Download Link Statistics';
				case '77806451':
					return 'PDA Access Restriction';
				case '77856881':
					return 'PDA Access Restriction';
				case '77829318':
					return 'PDA WooCommerce Integration';
				case '77836903':
					return 'PDA Membership Integration';
				case '77835198':
					return 'PDA Contact Forms Integration';
				case '77838452':
					return 'PDA Multisite ';
				case '77847335':
					return 'PDA S3 Integration';
				case '77847381':
					return 'PDA Robots.txt Integration';
				case '77861101':
					return 'PDA ActiveCampaign Integration';
				default:
					return '';
			}
		}

		public static function get_plugin_version() {
			$plugin_data = get_plugin_data( PDA_V3_PLUGIN_BASE_FILE );

			return $plugin_data['Version'];
		}

		/**
		 * @return array
		 */
		public static function get_current_role() {
			if ( is_multisite() && is_super_admin( wp_get_current_user()->ID ) ) {
				$current_role = array( 'administrator' );
			} else {
				$current_role = wp_get_current_user()->roles;
			}

			return $current_role;
		}

		public static function get_home_url_with_ssl() {
			return is_ssl() ? home_url( '/', 'https' ) : home_url( '/' );
		}

		/**
		 * Get mime type
		 *
		 * Copy from https://secure.php.net/manual/en/function.mime-content-type.php
		 */
		public static function pda_mime_content_type( $file_name ) {
			$mime_types = array(

				'txt'  => 'text/plain',
				'htm'  => 'text/html',
				'html' => 'text/html',
				'php'  => 'text/html',
				'css'  => 'text/css',
				'js'   => 'application/javascript',
				'json' => 'application/json',
				'xml'  => 'application/xml',
				'swf'  => 'application/x-shockwave-flash',
				'flv'  => 'video/x-flv',

				// images
				'png'  => 'image/png',
				'jpe'  => 'image/jpeg',
				'jpeg' => 'image/jpeg',
				'jpg'  => 'image/jpeg',
				'gif'  => 'image/gif',
				'bmp'  => 'image/bmp',
				'ico'  => 'image/vnd.microsoft.icon',
				'tiff' => 'image/tiff',
				'tif'  => 'image/tiff',
				'svg'  => 'image/svg+xml',
				'svgz' => 'image/svg+xml',

				// archives
				'zip'  => 'application/zip',
				'rar'  => 'application/x-rar-compressed',
				'exe'  => 'application/x-msdownload',
				'msi'  => 'application/x-msdownload',
				'cab'  => 'application/vnd.ms-cab-compressed',

				// audio/video
				'mp3'  => 'audio/mpeg',
				'qt'   => 'video/quicktime',
				'mov'  => 'video/quicktime',

				// adobe
				'pdf'  => 'application/pdf',
				'psd'  => 'image/vnd.adobe.photoshop',
				'ai'   => 'application/postscript',
				'eps'  => 'application/postscript',
				'ps'   => 'application/postscript',

				// ms office
				'doc'  => 'application/msword',
				'rtf'  => 'application/rtf',
				'xls'  => 'application/vnd.ms-excel',
				'ppt'  => 'application/vnd.ms-powerpoint',

				// open office
				'odt'  => 'application/vnd.oasis.opendocument.text',
				'ods'  => 'application/vnd.oasis.opendocument.spreadsheet',
			);

			$file_array = explode( '.', $file_name );
			$ext        = strtolower( array_pop( $file_array ) );
			if ( array_key_exists( $ext, $mime_types ) ) {
				return $mime_types[ $ext ];
			} elseif ( function_exists( 'finfo_open' ) ) {
				$file_info = finfo_open( FILEINFO_MIME );
				$mime_type = finfo_file( $file_info, $file_name );
				finfo_close( $file_info );

				return $mime_type;
			} else {
				return 'application/octet-stream';
			}
		}

		/**
		 * Return extensions has updates
		 *
		 * @return array
		 */
		public static function extensions_has_updates() {
			$extensions  = [
				'wp-pda-ip-block/wp-pda-ip-block.php',
				'pda-membership-integration/pda-membership-integration.php'
			];
			$updates     = get_plugin_updates();
			$plugin_keys = array_keys( $updates );
			$tmp         = array_map(
				function ( $extension ) use ( $plugin_keys, $updates ) {
					if ( false === array_search( $extension, $plugin_keys ) ) {
						return null;
					}
					$plugin = $updates[ $extension ];
					$name = $plugin->Name;
					return "<b>$name" . ' (' . $plugin->update->new_version . ')</b>';
				},
				$extensions
			);

			return array_filter( $tmp, function ( $item ) {
				return ! is_null( $item );
			} );
		}
	}
}
