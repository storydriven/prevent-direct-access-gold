<?php

if ( ! class_exists( 'PDA_Private_Hooks') ) {
    /**
     * Helper class to define the hooks existing in the Prevent Direct Access Gold plugin.
     *
     * Class PDA_Private_Hooks
     */
    class PDA_Private_Hooks {
        /**
         * Action check have bucket
         */
        const PDA_HOOK_CHECK_S3_HAS_BUCKET = 'pda_check_s3_has_bucket';

        /**
         * Action show file status under pda column
         */
        const PDA_HOOK_SHOW_STATUS_FILE_IN_PDA_COLUMN = 'pda_show_status_file_in_pda_column';

        /**
         * Action handle protect file in folder
         */
	    const PDA_HOOK_CUSTOM_HANDLE_PROTECTED_FILE = 'pda_custom_handle_protected_file';
    }
}
