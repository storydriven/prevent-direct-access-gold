
<a href="./admin.php?page=pda-gold" class="btn-end">End tour</a>
<div class="pda-wthrou-step intro" data-class="0">
	<h3>Welcome! Thanks for using <br>Prevent Direct Access Gold!</h3>
	<p>This quick tour will show you how to protect your private files. Don't worry, it's just a piece of cake!</p>
	<a data-step="pda-wthrou-step1" class="btn-next">Let's Get Started</a>

</div>
<div class="pda-wthrou-step pda-wthrou-step1" data-class="1">
	<h3>Step1: Click on "Media"</h3>
	<p>Under WordPress admin, click on "Media" and start protecting your private files</p>
	<a data-step="pda-wthrou-step2" class="btn-next">Next</a>
</div>
<div class="pda-wthrou-step pda-wthrou-step2" data-class="2">
	<h3>Step 2: Click on "File Protection Configuration"</h3>
	<p>Under Media, locate the file  you want to protect then click on "File Protection Configuration"</p>
	<a data-step="pda-wthrou-step1" class="btn-prev">Previous</a>
	<a data-step="pda-wthrou-step3" class="btn-next">Next</a>
</div>
<div class="pda-wthrou-step pda-wthrou-step3" data-class="3">
	<h3>Step 3: Click on "Protect this file"</h3>
	<p>Click on "Protect this file" to protect your file</p>
	<a data-step="pda-wthrou-step2" class="btn-prev">Previous</a>
	<a data-step="pda-wthrou-step4" class="btn-next">Next</a>
</div>
<div class="pda-wthrou-step pda-wthrou-step4" data-class="4">
	<h3>Step 4: Protected file access permission</h3>
	<p>Depending on your settings, your protected files are accessible to certain users by default. Click on the red button to customize the access permission.</p>
	<a data-step="pda-wthrou-step3"class="btn-prev">Previous</a>
	<a data-step="pda-wthrou-step5" class="btn-next">Next</a>
</div>
<div class="pda-wthrou-step pda-wthrou-step5" data-class="5">
	<h3>Step 4.1: Customize file access permission</h3>
	<p>Select who can access your this particular file, e.g. admins, logged-in users or no one.</p>
	<a data-step="pda-wthrou-step4" class="btn-prev">Previous</a>
	<a data-step="pda-wthrou-step6" class="btn-next">Next</a>
</div>
<div class="pda-wthrou-step pda-wthrou-step6" data-class="6">
	<h3>Step 5: Create private download links</h3>
	<p>Create and expire new private links. Use these private download links to share your private files with certain people.</p>
	<a data-step="pda-wthrou-step5" class="btn-prev">Previous</a>
	<a data-step="pda-wthrou-step7" class="btn-next">Next</a>
</div>
<div class="pda-wthrou-step pda-wthrou-step7" data-class="7">
	<h3>Resources:</h3>
	<p>For more information, please check out these tutorials and blog posts below:</p>
	<ul>
		<li><a href="https://preventdirectaccess.com/docs/original-vs-private-download-links/">Original vs Private Links</a></li>
		<li><a href="https://preventdirectaccess.com/docs/7-common-mistakes-with-prevent-direct-access-its-extensions/"> 5 common mistakes when using Prevent Direct Access Gold</a></li>
		<li><a href="https://preventdirectaccess.com/documentation/basic-troubleshooting/">Basic Troubleshooting</a></li>
		<li><a href="https://preventdirectaccess.com/docs/file-access-permission-priority/">File Access Permission Priority</a></li>
		<li><a href="https://preventdirectaccess.com/faq/">Frequently asked questions</a></li>
	</ul>
	<a href="./admin.php?page=pda-gold" class="btn-complete">Complete</a>
</div>
